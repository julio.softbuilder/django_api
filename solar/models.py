from django.db import models
from uuid import uuid4

def updatePdf(instance, filename):
    return f"{instance.id_fatura}-{filename}"

class UpdateFatura(models.Model):
    id_fatura = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    create_at = models.DateField(auto_now_add=True)
    file = models.FileField(upload_to=updatePdf, blank=True, null=True)
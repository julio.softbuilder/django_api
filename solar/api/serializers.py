from django.db.models import fields
from rest_framework import serializers
from solar import models

class UpdateFaturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UpdateFatura
        fields = '__all__'
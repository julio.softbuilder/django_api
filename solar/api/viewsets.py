from django.shortcuts import render

# from .serializers import PostSerializer
# from .models import Post
from solar.api.serializers import UpdateFaturaSerializer
from solar.models import UpdateFatura
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status

class PostView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    # METODO POST
    def post(self, request, *args, **kwargs):
        posts_serializer = UpdateFaturaSerializer(data=request.data)
        

        # arquivo que o usuario fez o upload --> request.data['file']
        print(request.data['file'])
        
        up_file = request.FILES['file']
        up_pathfile = ''
        
        if posts_serializer.is_valid():
            posts_serializer.save()
            print("---- entrou -----")
            # data_name=up_file.name
            # data_path=up_pathfile
            
            # popular variavel data com resposta da conversao
            data={}
            data["txt"]="data_name"
            # data["file_path"]=data_path

            
            # response 
            return Response(data=posts_serializer.data, status=status.HTTP_200_OK)
        else:
            print('error', posts_serializer.errors)
            return Response(posts_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get(self, request, *args, **kwargs):
        posts = UpdateFatura.objects.all()
        return Response(data=posts, status=status.HTTP_200_OK)